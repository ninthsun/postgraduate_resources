# 考研资料
## 考研英语资料
1. 百度网盘
    1. 2020考研：https://pan.baidu.com/share/init?surl=VQZ1w5h7TFmfdZCgfFtiYQ 密码:rpz8
    2. 2019考研：https://pan.baidu.com/share/init?surl=gV51Jgw3WnW0tW0iJ4njSA 密码:ufq2

## 考研数学资料
1. 百度网盘
    1. 2020考研：https://pan.baidu.com/s/1BF8vYOwsH_5PNPmypVqvoA 密码:nb1a
    2. 2019考研：https://pan.baidu.com/share/init?surl=zUvzCbEz20dfhKlV3yHpZA 密码:jwjc 

## 考研政治资料
1. 百度网盘
    1. 2020考研：https://pan.baidu.com/wap/init?surl=EaFdeOCfK1Xu5UA5RlqyBQ 密码:5bit
    2. 2019考研：https://pan.baidu.com/share/init?surl=QFtNr0-dgaEGSA91b8Qekw 密码:2mon

## 一个每天持续更新资料的微信公众号
>公众号名:考研伴

## 其他
* 有同学想共享资料的可以注册个gitlab账号在这个项目下的Issues里面说就行，然后我闲了看到会更新。
* 不光考研资料，其他也行，论文啊，毕设呀，破解软件呀，娱乐呀什么的都行。